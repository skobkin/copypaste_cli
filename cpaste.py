#!/usr/bin/env python3
__author__ = 'skobkin'
version = '1.0'

import urllib.request
import urllib.response
import urllib.parse
import urllib.error
from optparse import OptionParser


def post_request(url, query_data):
    print(query_data)
    try:
        query = urllib.parse.urlencode(query_data).encode('ascii')
        request = urllib.request.Request(url, query)
        response = urllib.request.urlopen(request)
    except Exception as ex:
        print(ex)
        return False
    else:
        try:
            data = response.read().decode(encoding='utf_8', errors='replace')
        except Exception as ex:
            print(ex)
            return False
        else:
            return data


def get_request(url, query_data={}):
    try:
        if len(query_data) > 0:
            query_encoded = urllib.parse.urlencode(query_data)
            response = urllib.request.urlopen(url, query_encoded)
        else:
            response = urllib.request.urlopen(url)
    except urllib.error.HTTPError as error:
        print('HTTP Error:', error.reason)
        return False
    except Exception as ex:
        print(ex)
        return False
    else:
        try:
            data = response.read().decode(encoding='utf_8', errors='replace')
        except Exception as ex:
            print(ex)
            return False
        else:
            return data


class CopyPaste():
    lang_list = {}
    # No trailing slash
    service_url = 'http://cp.skobkin.ru'

    code = None
    lang = 0
    expire = 300
    author = None
    private = False

    def __init__(self, code=None):
        self.code = code

    def get_languages(self):
        import json

        json_data = get_request('{}/api/langlist/json'.format(self.service_url))

        try:
            data = json.loads(json_data)
            for lang in data['langs']:
                self.lang_list[lang['file']] = {
                    'id': lang['id'],
                    'title': lang['name']
                }
                #print(self.lang_list[lang['file']])
        except Exception as ex:
            print(ex)
            return False
        else:
            return True

    def set_lang(self, lang, mode='alias'):
        if len(self.lang_list) < 1:
            self.get_languages()

        if mode == 'alias':
            if lang in self.lang_list:
                self.lang = self.lang_list[lang]['id']
                #print(self.lang_list[lang]['id'])
                return True
            else:
                return False
        elif mode == 'id':
            self.lang = lang
            return True

    def send_paste(self):
        query_data = {
            'code': paste,
            'expire': int(opts.expire)
        }

        # Author
        if self.author is not None:
            query_data['author'] = self.author

        # Language
        if self.lang != 0:
            query_data['lang'] = self.lang

        # Private
        if self.private:
            query_data['private'] = 1

        # Expiration
        if str(self.expire).isnumeric():
            query_data['expire'] = self.expire

        return post_request('{url}/api/add/plain/file'.format(url=self.service_url), query_data)


# Script start
if __name__ == '__main__':
    usage = 'usage:\n' \
            '%prog [options] < file\n' \
            'cat file | %prog [options]'

    optp = OptionParser(usage=usage, version='CopyPaste CLI {}'.format(version))

    optp.add_option('-l', '--language', help='set language', dest='language', default='plain')
    optp.add_option('-e', '--expire', help='set expire time', dest='expire', default=300)
    optp.add_option('-p', '--private', help='set privacy mode', dest='private', action='store_true', default=False)
    optp.add_option('-a', '--author', help='set author', dest='author', default=None)

    opts, args = optp.parse_args()

    paste = ''

    # Reading stdin
    while True:
        try:
            paste += input() + '\n'
        except Exception as ex:
            break

    if len(paste) > 0:
        cp = CopyPaste(paste)
        cp.author = opts.author
        cp.expire = opts.expire
        cp.private = opts.private

        if opts.language != 'plain':
            cp.get_languages()
            cp.set_lang(opts.language)

        link = cp.send_paste()

        if link is not False:
            print(link)
        else:
            print('Error')
    else:
        print('No paste data')
